# everything from '#' till the end of the line is a comment

[chain_example_4]
# all following rules will be appended to the chain with this name
# chain name is a single word, number only names are not allowed
# default final action is "drop"

[vlan999] # chains names "vlan[0-9]+" are automatically attached to that vlan
# traffic for other vlans are passed by default
# all other chain names are supplemental
# chain consists of filter rules, each rule is defined on a single line
# each filter line contains action and optional filter keys
# filter keys are:
#   vlan_id <vlan>
#   vlan_ethtype ip|ipv6
#   ip_proto <proto>
#   src_ip|dst_ip <ip|net>
#   src_port|dst_port <port>
#   ip_ttl <ttl>[/<bitmask>]
# actions are:
#   action pass
#   action drop
#   action trap
#   action goto [chain] # brackets are part of the syntax
# rule can have explicit protocol specification
# otherwise it is derived from the keys or default "ip" is used
# possible set of keys depends on the protocol
#   proto all|802.1Q|ip|ipv6
src_ip 2001:db8:cafe::/48 action pass # proto -> ipv6
src_ip 10.0.0.0/8 action pass # proto -> ip
vlan_id 100 # proto -> 802.1Q
# vlan_ethtype is also derived from the keys with no default value
vlan_id 100 src_ip 10.0.0.0/8 action pass # proto -> 802.1Q, vlan_ethtype -> ip
vlan_id 100 src_ip 2001:db8:cafe::/48 action pass # proto -> 802.1Q, vlan_ethtype -> ipv6
proto 802.1Q vlan_id 100 vlan_ethtype ipv6 src_ip 2001:db8:cafe::/48 action pass # same as above
proto 802.1Q src_ip 2001:db8:cafe::/48 action pass # vlan_ethtype -> ipv6
dst_ip 10.0.0.0/8 action drop
action pass src_ip 2001:db8:cafe::/48 # order of keys and action is not important
dst_ip 172.16.0.1 action goto [chain_example_4]
# chains that are defined later can be refered too
ip_proto tcp dst_ip 172.16.0.2 src_port 179 action goto [vlan999_direct]
dst_ip 172.16.0.2 action drop
# implicit drop will be added here
#proto all action drop

[vlan999_direct]

action drop # default ip protocol with no keys to determine automatically
proto ipv6 action drop # set proto manually for ipv6

[chain_example_4] # if chain appears several times, rules are appended
ip_proto icmp action pass

123: protocol all action pass # rules can be prefixed with specific preference
# the syntax is nonnegative integer followed by colon without spaces in between
 10: protocol all action pass # but spaces are allowed before the integer
vlan_ethtype ipv6 # this rule will have preference 11 (previous rule +1)
8: # preference without a rule sets the preference for the following rule in this chain
vlan_ethtype ip ip_proto tcp # this rull will have preference 8
# default preferences are started from 1 and incremented by 1 if no specific
# preference is set, the preferences are independent for different chains
[chain_a]
10: vlan_id 100 action pass
[chain_b]
20: vlan_id 200 action pass
[chain_a]
vlan_id 101 action pass # preference 11
# the preferences are not mapped directly to the preferences in tc, but are
# only # used to sort the rule, thus only order of the rules matters while
# loading the rules, these chains are considered similar:
[variant_a]
vlan_id 1 action pass
vlan_id 2 action pass
[variant_b]
20: vlan_id 2 action pass
10: vlan_id 1 action pass
# if rules have the same preference, the are loaded under the same preference
# with different tc handles in order of appearance
[multiple_handle]
5: vlan_id 1 action pass
5: vlan_id 2 action drop
# the rules having the same preference must have the same protocol
[error]
2: protocol ip ip_proto icmp action pass
2: protocol all action pass # this is not allowed
